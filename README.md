# Pluralsislab

Este projecto tem como finalidade criar um ambiente de aprendizado com ferramentas DevOps mais utilizadas no mercado tais como: (Vagrant Kubernetes Terraform Ansible...). O cluster kubernetes será criado em máquinas virtuais rodando sobre KVM com provisionador Libvirt o mesmo poderá ser criado usando, Ansible e ShellScript com Vagrant, a ideia é poder mostrar como criar um cluster k8s on-primese de diferentes forma e o provisionamento de Infraestrutura com Ansible, Vagrant e Terraform por cima do hipervisor KVM.


## Preparar o ambiente no manjaro

```bash
sudo pacman -Syy
sudo reboot 
sudo pacman -S archlinux-keyring sshpass vagrant
sudo pacman -S qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat
```

* Also install ebtables  and iptables packages:

```bash
sudo pacman -S ebtables iptables
```

### Instalar a libguestfs
```bash
sudo pacman -S libguestfs
```

### Inicializar o serviço do KVM libvirt

```bash
sudo systemctl enable libvirtd.service
sudo systemctl start libvirtd.service
```

### Activar a conta do utilizador normal para usar KVM

Since we want to use our standard Linux user account to manage KVM, let’s configure KVM to allow this.

Open the file /etc/libvirt/libvirtd.conf for editing with your text editor. I am using vim.

```bash
sudo pacman -S vim
sudo vim /etc/libvirt/libvirtd.conf
```

```config
# linha 85
unix_sock_group = "libvirt"

# linha 102
unix_sock_rw_perms = "0770"
```

### Adicionar conta de utilizado no grupo libvirt

```bash
sudo usermod -a -G libvirt $(whoami)
newgrp libvirt
```

### Reinicializar libvirt deamon

```bash
sudo systemctl restart libvirtd.service
```

### Activar virtualização nested (opcional)

Nested Virtualization feature enables you to run Virtual Machines inside a VM. Enable Nested virtualization for kvm_intel / kvm_amd by enabling kernel module as shown.

```bash
### Intel Processor ###
sudo modprobe -r kvm_intel
sudo modprobe kvm_intel nested=1

### AMD Processor ###
sudo modprobe -r kvm_amd
sudo modprobe kvm_amd nested=1
```

* To make this configuration persistent,run:

```bash
echo "options kvm-intel nested=1" | sudo tee /etc/modprobe.d/kvm-intel.conf
```

*Confirm that Nested Virtualization is set to Yes:

```bash
### Intel Processor ###
$ systool -m kvm_intel -v | grep nested
    nested              = "Y"
    nested_early_check  = "N"
$ cat /sys/module/kvm_intel/parameters/nested 
Y

### AMD Processor ###
$ systool -m kvm_amd -v | grep nested
    nested              = "Y"
    nested_early_check  = "N"
$ cat /sys/module/kvm_amd/parameters/nested 
Y
```

# O que é o kubernetes?
Kubernetes (K8s) é um produto Open Source utilizado para automatizar a implantação, o dimensionamento e o gerenciamento de aplicativos em contêiner.
* Todo accção que acontece no cluster passa pelo apiserver do kubernetes, o etcd é o banco de dados
* kubeScaduler - é o responsável por selecionar o node adequado
* kubelet roda em todos node master e workers

## que é um Pod?
É a menor unidade dentro de cluster k8s, a diferença que tempos de um Pod por um Container é que o Pod vai dividir o namespace (compartilhar) em os container ele pode comportar mais de um container, mais a conselha-se um container por Pod ou no minimo dois.

## Controller
é o responsável por controlar (objecto, resource) toda a orquestração ele comunicaºse com apiserver do kubernetes para colectar informações do cluster através do replicaSet se alguma coisa de errado a conter como replicaSet o controller responsável por tomar decisão é Deployment.
Nas camadas de Objectos do kubernetes temos os:
-> [Deployment [Service [ReplicaSet [DeamonSet [Pod [Ingress]]]]]]

## Deployment
É um dos principais controller ele controla os ReplicaSet
Dentro do kubernetes para para fazer o deploy de uma app temos que os seguintes passos:
1. Create a Deployment (create auto a replicaSet)
2. Create a Service (Para ter acesso externo Bind Port to Deployment as Replicas)


# Run cluster

```bash
git clone https://gitlab.com/angolarti/pluralsislab.git
```

```bash
cd pluralsislab/kubernetes
```

```bash
vagrant plugin install vagrant-libvirt
vagrant plugin install vagrant-sshfs
```

```bash
vagrant up
```