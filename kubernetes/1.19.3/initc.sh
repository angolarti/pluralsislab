#!/bin/bash

set -e

USER_HOME=/home/vagrant

# Download images
kubeadm config image pull

# Initialize cluster
kubeadm init --v=5

# To start using your cluster
mkdir -p $USER_HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $USER_HOME/.kube/config
chown vagrant:vagrant $USER_HOME/.kube/config

# Alternatively, if you are the root user, you can run
export KUBECONFIG=/etc/kubernetes/admin.conf

modprobe overlay br_netfilter bfq ip_vs_rr ip_vs_wrr ip_vs_sh nf_conntrack_ipv4 ip_vs

# Should now deploy a pod network to the cluster...
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

echo -n "Get join token"
echo -n "========================================================================="
TOKEN_JOIN_WORKERS=$(kubeadm token create --print-join-command)

echo -n "$TOKEN_JOIN_WORKERS --v=5 --ignore-preflight-errors=all" > token/.token_join
echo -n "============================= TOKEN_JOIN_WORKERS ============================================"
