#!/bin/bash

set -e

# Disable swap on all the Nodes
swapoff -a
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

# Enable iptables Bridged Traffic
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

# sysctl params required by setup, params persist across reboots
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
sudo sysctl --system

# Update the apt package index and install packages needed to use the Kubernetes apt repository
apt update
apt install -y apt-transport-https ca-certificates curl

# Install Docker container runtime
curl -fsSL https://get.docker.com | bash

# Configure container runtime deamon by EOF (End Of File)..."
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

# Create docker service for systemd..."
mkdir -p /etc/systemd/system/docker.service.d

# reload daemon..."
systemctl daemon-reload

# Restart docker..."
systemctl restart docker

# Check cgroup in docker info..."
docker info | grep -i cgroup

# Upgrade distribuition"
apt update && apt full-upgrade -y

# Install bash completion"
apt install -y bash-completion

# Add vagrant user to docker group"
usermod -aG docker vagrant

# Show containers..."
docker container ls

# Download the Google Cloud public signing key
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - 

# Add the Kubernetes apt repository
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list

# Update apt package index, install kubelet, kubeadm and kubectl, and pin their version
apt update
apt install -y kubeadm=1.19.16-00 kubelet=1.19.16-00 kubectl=1.19.16-00
apt-mark hold kubelet kubeadm kubectl

echo "Add bash completion for kubectl"
kubectl completion bash > /etc/bash_completion.d/kubectl

echo "Force kubectl bash completion active now"
source <(kubectl completion bash)


# Clean apt cache
apt clean
rm -rf /var/lib/apt/lists/*
apt clean
apt update
