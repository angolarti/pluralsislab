#!/bin/bash

set -e

USER_HOME=/home/vagrant

echo -n "Download images of the components by cluster..."
sudo kubeadm config images pull

IPADDR="192.168.56.10"
NODENAME=$(hostname -s)

echo -n "Initialize cluster..."
if [ ! -d "$USER_HOME/token" ]
then
    mkdir $USER_HOME/token
fi

sudo kubeadm init --apiserver-advertise-address=$IPADDR \
  --apiserver-cert-extra-sans=$IPADDR \
  --pod-network-cidr=192.168.0.0/16 \
  --node-name $NODENAME --v=5 --ignore-preflight-errors=all

echo -n "Get join token"
echo -n "========================================================================="
TOKEN_JOIN_WORKERS=$(kubeadm token create --print-join-command)

echo -n "$TOKEN_JOIN_WORKERS --v=5 --ignore-preflight-errors=all" > token/.token_join
echo -n "============================= TOKEN_JOIN_WORKERS ============================================"

echo -n "configure runtime container for normal user..."
mkdir -p $USER_HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $USER_HOME/.kube/config
# sudo chown $(id -u):$(id -g) $USER_HOME/.kube/config
chown vagrant:vagrant $USER_HOME/.kube/config

### Alternatively, if you are the root user, you can run

export KUBECONFIG=/etc/kubernetes/admin.conf

# Install Calico Network Plugin for Pod Networking
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
