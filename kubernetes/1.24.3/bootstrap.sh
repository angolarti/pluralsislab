#!/bin/bash

set -e

# Disable Firewall
sudo ufw disable

# Enable iptables Bridged Traffic
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

# sysctl params required by setup, params persist across reboots
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
sudo sysctl --system

# Disable swap on all the Nodes
sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

# Install Containerd Container Runtime On All The Nodes

# Install the required packages for container runtime
sudo apt update -y
sudo apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# Install the containerd
sudo apt install containerd -y

# Forwarding IPv4 and letting iptables see bridged traffic
sudo modprobe overlay br_netfilter bfq ip_vs_rr ip_vs_wrr ip_vs_sh nf_conntrack_ipv4 ip_vs

cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
bfq
ip_vs_rr
ip_vs_wrr
ip_vs_sh
nf_conntrack_ipv4
ip_vs
EOF

# Configurar o containerd
mkdir -p /etc/containerd
containerd config default > /etc/containerd/config.toml
sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml

systemctl enable containerd
systemctl restart containerd
systemctl status containerd

# Enable kubectl autocompletion
echo 'source <(kubectl completion bash)' >> ~/.bashrc
echo 'alias k=kubectl' >> ~/.bashrc
echo 'complete -o default -F __start_kubectl k' >> ~/.bashrc

# Install Kubeadm & Kubelet & Kubectl on all Nodes
sudo apt install -y apt-transport-https ca-certificates curl
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

# Add the GPG key and apt repository.
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

# Update apt and install kubelet, kubeadm, and kubectl.
sudo apt update -y
sudo apt install -y kubelet kubeadm kubectl

# Add bash completion for kubectl
kubectl completion bash > /etc/bash_completion.d/kubectl

# Force kubectl bash completion active now
source <(kubectl completion bash)